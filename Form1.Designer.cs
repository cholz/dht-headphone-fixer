﻿namespace DhtHeadphoneFixer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.SystemTrayMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.normalSettingsItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dhtSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.close_application = new System.Windows.Forms.ToolStripMenuItem();
            this.SystemTrayNotifyer = new System.Windows.Forms.NotifyIcon(this.components);
            this.SystemTrayMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // SystemTrayMenu
            // 
            this.SystemTrayMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.normalSettingsItem,
            this.dhtSettings,
            this.toolStripSeparator1,
            this.close_application});
            this.SystemTrayMenu.Name = "contextMenuStrip1";
            this.SystemTrayMenu.Size = new System.Drawing.Size(160, 76);
            this.SystemTrayMenu.Text = "Test";
            this.SystemTrayMenu.Opening += new System.ComponentModel.CancelEventHandler(this.SystemTrayMenu_Opening);
            // 
            // normalSettingsItem
            // 
            this.normalSettingsItem.AccessibleName = "normalSettings";
            this.normalSettingsItem.Name = "normalSettingsItem";
            this.normalSettingsItem.Size = new System.Drawing.Size(159, 22);
            this.normalSettingsItem.Text = "Normal Settings";
            this.normalSettingsItem.Click += new System.EventHandler(this.normalSettingsItem_Click);
            // 
            // dhtSettings
            // 
            this.dhtSettings.AccessibleName = "dhtSettings";
            this.dhtSettings.Name = "dhtSettings";
            this.dhtSettings.Size = new System.Drawing.Size(159, 22);
            this.dhtSettings.Text = "DHT Settings";
            this.dhtSettings.Click += new System.EventHandler(this.dhtSettings_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(156, 6);
            // 
            // close_application
            // 
            this.close_application.AccessibleName = "close_application";
            this.close_application.Name = "close_application";
            this.close_application.Size = new System.Drawing.Size(159, 22);
            this.close_application.Text = "Close";
            this.close_application.Click += new System.EventHandler(this.close_application_Click);
            // 
            // SystemTrayNotifyer
            // 
            this.SystemTrayNotifyer.ContextMenuStrip = this.SystemTrayMenu;
            this.SystemTrayNotifyer.Icon = ((System.Drawing.Icon)(resources.GetObject("SystemTrayNotifyer.Icon")));
            this.SystemTrayNotifyer.Text = "Audio Settings Changer";
            this.SystemTrayNotifyer.Visible = true;
            this.SystemTrayNotifyer.MouseClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseClick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(558, 177);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.Opacity = 0D;
            this.ShowInTaskbar = false;
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.SystemTrayMenu.ResumeLayout(false);
            this.ResumeLayout(false);


        }
        
        #endregion

        private System.Windows.Forms.ContextMenuStrip SystemTrayMenu;
        private System.Windows.Forms.ToolStripMenuItem normalSettingsItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem dhtSettings;
        private System.Windows.Forms.NotifyIcon SystemTrayNotifyer;
        private System.Windows.Forms.ToolStripMenuItem close_application;


        #region

        private static int WM_QUERYENDSESSION = 0x11;

        public void setDHTChecked()
        {
            normalSettingsItem.Image = null;
            dhtSettings.Image = Properties.Resources.CheckedIcon.ToBitmap();
        }

        public void setNormalChecked()
        {
            dhtSettings.Image = null;
            normalSettingsItem.Image = Properties.Resources.CheckedIcon.ToBitmap();
        }

        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            if (m.Msg == WM_QUERYENDSESSION)
            {
                registry.loadNormalConfig();
            }

            // If this is WM_QUERYENDSESSION, the closing event should be
            // raised in the base WndProc.
            base.WndProc(ref m);

        } //WndProc 

        #endregion
    }
}

