﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using System.Security.AccessControl;
using System.Security.Principal;
using System.IO;
using System.Diagnostics;

using System.Security.AccessControl;
using System.Security.Principal;
using Microsoft.Win32;

using System;
using System.Runtime.InteropServices;

namespace DhtHeadphoneFixer
{
    class RegistryModel
    {
        private RegistryKey getSoundConfig()
        {
            RegistryKey audioDevicesKey = null;
            RegistryKey rk = null;


            if (Environment.Is64BitOperatingSystem == true)
            {
                audioDevicesKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry64);
            }
            else
            {
                audioDevicesKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry32);
            }


        
           rk = audioDevicesKey.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\MMDevices\\Audio\\Render", RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.ReadKey | RegistryRights.SetValue);
               
         
            return rk;
        }


        private RegistryKey getAsusSpeakersKey()
        {

            RegistryKey audioDevicesKey = getSoundConfig();


            String[] subkeynames = audioDevicesKey.GetSubKeyNames();
            foreach (String subkeyname in subkeynames)
            {
                Boolean found_card = false;
                Boolean found_analog = false;

                RegistryKey subkey = audioDevicesKey.OpenSubKey(subkeyname + "\\Properties", RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.ReadKey|RegistryRights.SetValue);
                String[] valueNames = subkey.GetValueNames();
                foreach (String valueName in valueNames)
                {
                    Object value = (Object)subkey.GetValue(valueName, "NoValue");
                    if (value is String)
                    {
                        if (value.Equals("ASUS Xonar Essence STX Audio Device"))
                        {
                            found_card = true;
                        }
                       
                    } else 
                    {
                        if (valueName.Equals("{1da5d803-d492-4edd-8c23-e0c0ffee7f0e},3"))
                        {
                            found_analog = true;
                        }
                    }
                }

                if (found_analog && found_card)
                {
                   return subkey;
                }
            }
            return null;
        }

        private object getAudioKey()
        {
            string user = Environment.UserDomainName + "\\" + Environment.UserName;
            RegistrySecurity rs = new RegistrySecurity();
            RegistryKey audioDevicesKey = getAsusSpeakersKey();
            if (audioDevicesKey != null)
            {
                RegistrySecurity oldSec = audioDevicesKey.GetAccessControl();
                SecurityIdentifier sid = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                NTAccount account = sid.Translate(typeof(NTAccount)) as NTAccount;

                using (audioDevicesKey)
                {
                    rs = audioDevicesKey.GetAccessControl();
                    return audioDevicesKey.GetValue("{1da5d803-d492-4edd-8c23-e0c0ffee7f0e},0");
                }
            }

            return null;
        }

        private void setAudioKey(int value)
        {
            string user = Environment.UserDomainName + "\\" + Environment.UserName;
            RegistrySecurity rs = new RegistrySecurity();
            RegistryKey audioDevicesKey = getAsusSpeakersKey();
            if (audioDevicesKey != null)
            {
                RegistrySecurity oldSec = audioDevicesKey.GetAccessControl();
                SecurityIdentifier sid = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                NTAccount account = sid.Translate(typeof(NTAccount)) as NTAccount;

                using (audioDevicesKey)
                {
                    rs = audioDevicesKey.GetAccessControl();
                    audioDevicesKey.SetValue("{1da5d803-d492-4edd-8c23-e0c0ffee7f0e},0", value, RegistryValueKind.DWord);
                }
            }
        }

        public void loadNormalConfig()
        {
            setAudioKey(1);
        }

        public void loadDhtConfig()
        {
            setAudioKey(3);
        }


        #region public functions
        public bool isNormalConfig()
        {

            var value = getAudioKey();
            if ((int)value == 1)
            {
                return true;
            }

            return false;
        }

        public bool isDHTConfig()
        {
            var value = getAudioKey();
            if ((int)value == 3)
            {
                return true;
            }
            return false;
        }

        #endregion
    }
}
