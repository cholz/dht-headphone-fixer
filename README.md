# dht-headphone-fixer

Fixes the DHT4 virtual surround mode for headphones with UNI Xonar drivers.


## Installation and Usage ##
- Install Uni-Xonar drivers with DHT4 enabled.
- Use the analog "speaker" output
- Enable Headphone Analog Output and 8 Channel System Input in
- Enable Headphone Surround virtualization.
- Run the exe (a binary is included: https://gitlab.com/cholz/dht-headphone-fixer/tags)
- Use the System tray icon to enable DHT mode (you have to listen, if its working. Currently no icon will show you which setting is active)
- !!! Do not change the Speaker name in the windows audio settings

## Features ##

On Shutdown the normal configuration will be loaded, so that the sound settings will work after reboot.

## Requirements ##
The app must run with elevated rights, which prevents it to run on startup.

## TODO ##
Well... lots and lots and lots of things





## Licences

Checked Icon by

[Dave Gandy](https://www.flaticon.com/authors/dave-gandy)

