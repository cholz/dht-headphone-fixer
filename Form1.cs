﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Principal;

using System.Diagnostics;


namespace DhtHeadphoneFixer
{
    public partial class Form1 : Form
    {
        RegistryModel registry = new RegistryModel();
      
        public Form1()
        {
            InitializeComponent();
            WindowState = FormWindowState.Minimized;
            Hide();
           
            if (!IsAdmin())
            {
                // Launch itself as administrator
                ProcessStartInfo proc = new ProcessStartInfo();
                proc.UseShellExecute = true;
                proc.WorkingDirectory = Environment.CurrentDirectory;
                proc.FileName = Application.ExecutablePath;
                proc.Verb = "runas";
                
                try
                {
                    Process.Start(proc);
                }
                catch
                {
                    // The user refused to allow privileges elevation.
                    // Do nothing and return directly ...
                    return;
                }

                Application.Exit();  // Quit itself
            }

        }

        private void LoadCurrentSoundSettings()
        {
            if (registry.isNormalConfig())
            {
                setNormalChecked();
            }

            if (registry.isDHTConfig())
            {
                setDHTChecked();
            }
        }

        private void SystemTrayMenu_Opening(object sender, CancelEventArgs e)
        {
            
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {

            SystemTrayMenu.Show(Control.MousePosition);
            SystemTrayMenu.Focus();
   
        }

        private void close_application_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Opacity = 0.0f;
            this.ShowInTaskbar = false;
            this.Visible = false;
            LoadCurrentSoundSettings();
        }

        private void normalSettingsItem_Click(object sender, EventArgs e)
        {
            registry.loadNormalConfig();
            setNormalChecked();
        }

        private void dhtSettings_Click(object sender, EventArgs e)
        {
            registry.loadDhtConfig();
            setDHTChecked();
        }

        static internal bool IsAdmin()
        {
            WindowsIdentity id = WindowsIdentity.GetCurrent();
            WindowsPrincipal p = new WindowsPrincipal(id);
            return p.IsInRole(WindowsBuiltInRole.Administrator);
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
          
        }

    }
}
